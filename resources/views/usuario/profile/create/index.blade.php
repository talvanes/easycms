<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Register</title>
</head>
<body>
	<h1>Sign up</h1>
	
	<!-- Mensagem de erro -->
	@if ( Session::has('error') )
	<p style="color: red;">
		Erro: {{ Session::get('error') }}
	</p>
	@elseif ( Session::has('alert') )
	<p style="color: #d28420;">
		Atenção: {{ Session::get('alert') }}
	</p>
	@endif

	<form method="post" action="{!! route('profile.store') !!}">
		{!! csrf_field() !!}
		<label for="inputName">
			Nome: <input type="text" id="inputName" name="name" autofocus />
		</label>
		<br />
		<label for="inputLogin">
			Login: <input type="text" id="inputLogin" name="login" />
		</label>
		<br />
		<label for="inputEmail">
			Email: <input type="email" id="inputEmail" name="email" />
		</label>
		
		<br />
		
		<label for="inputSenha">
			Senha: <input type="password" id="inputSenha" name="password" />
		</label>
		<!-- <br />
		<label for="inputSenhaConfirm">
			Confirmar Senha: <input type="password" id="inputSenhaConfirm" name="password" />
		</label> -->
		<br />
		<label for="selectLevel">
			Nível de acesso: <br />
			<select name="level">
				@foreach($levels as $level => $desc)
				{-- o usuário é administrador? criar opção *admin* --}
					@if ( $level < 3 )
					<option value="{{ $level }}">{{ $desc }}</option>
					@endif
				@endforeach
			</select>
		</label>
		
		<br />
		
		<button type="submit">Sign up</button>
	</form>
	
	@include('usuario.profile.create.js')
</body>
</html>