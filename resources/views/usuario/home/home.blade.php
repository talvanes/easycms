<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Home page</title>
</head>
<body>
	<h1>Login</h1>
	
	<!-- Mensagem de erro -->
	@if ( Session::has('error') )
	<p style="color: red;">
		Erro: {{ Session::get('error') }}
	</p>
	@elseif ( Session::has('alert') )
	<p style="color: #d28420;">
		Atenção: {{ Session::get('alert') }}
	</p>
	@endif
	
	<form action="{{ route('usuario.auth') }}" method="post">
		{{ csrf_field() }}
		<label for="inputLogin">
			Login: <input type="text" id="inputLogin" name="login" autofocus />
		</label>
		<label for="inputSenha">
			Senha: <input type="password" id="inputSenha" name="password" />
		</label>
		<button type="submit">Login</button>
	</form>
	
	<a style="display: inline-block; float: right;" href="{!! route('profile.create') !!}">Sign up</a>
	
</body>
</html>