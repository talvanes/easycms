<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Área restrita</title>
</head>
<body>
	<h1>Bem-vindo, {{ $usuario->name }}!</h1>
	<p style="text-align: right;">
		@if ( $usuario->level == 3 )
		<a href="{{ route('profile.index') }}">Profile</a> |
		@endif
		<a href="{{ route('usuario.logout') }}">Sair</a>
	</p>
</body>
</html>