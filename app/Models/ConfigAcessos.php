<?php

namespace EasyCMS\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigAcessos extends Model
{
	/** @var string */
    protected $table = 'ConfigAcessos';
    
    /** @var string */
    protected $primaryKey = 'id';
    
    /** @var array */
    protected $fillable = [
    	'name', 'description',
    ];
    
    /** @var string */
    public $timestamps = false;
    
    /** @var string */
    public static $snakeAttributes = false;
}
