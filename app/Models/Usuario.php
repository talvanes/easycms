<?php

namespace EasyCMS\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuario extends Authenticatable
{
	/** @var string */
    protected $table = 'Usuarios';
	
	/** @var string */
	protected $primaryKey = 'id';
	
	/** @var array */
	protected $fillable = [
		'name',
		'login',
		'email',
		'password',
		'remember_token',
		'status',
		'level',
	];
	
	/** @var array */
	protected $hidden = [ 'password', 'remember_token' ];
	
	/** @var bool */
	public $timestamps = false;
	
	/** @var boolean */
	public static $snakeAttributes = true;
}
