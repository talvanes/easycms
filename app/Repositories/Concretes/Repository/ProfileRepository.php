<?php namespace EasyCMS\Repositories\Concretes\Repository;

use EasyCMS\Repositories\RepositoryAbstract;
use EasyCMS\Models\Usuario;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Container\Container as App;
use EasyCMS\Exceptions\ProfileException;
use EasyCMS\Repositories\Concretes\Criteria\UsersNotAdmin;
use Illuminate\Database\Eloquent\Collection;

class ProfileRepository extends RepositoryAbstract {
	
	/** @var Guard */
	protected $guard;
	/** @var Usuario */
	protected $profile;
	
	/**
	 * ProfileRepository's default constructor.
	 * @param App $app
	 * @param Guard $guard
	 */
	public function __construct(App $app, Guard $guard, Usuario $usuario, Collection $criteria){
		parent::__construct($app, $criteria);
		$this->guard = $guard;
		$this->usuario = $usuario;
	}
	
	/**
	 * [Usuario::class]: That's the entity's name.
	 * 
	 * {@inheritDoc}
	 * @see \EasyCMS\Repositories\RepositoryAbstract::entity()
	 */
	public function entity() {
		return Usuario::class;
	}
	
	
	// TODO: implement all the methods needed by ProfileController 
	
	/**
	 * According to the user logged in, this method lists all the records
	 * 
	 * @throws ProfileException
	 */
	public function listAllUsers() {
		
		# acesso restrito a administradores (deve haver erro)
		if (!$this->guard->check() || $this->guard->user()->login !== 'admin'){
			throw new ProfileException("Acesso restrito a administradores!");
		}
		
		# obtendo os usuários regulares
		$users = $this->getByCriteria(new UsersNotAdmin())->all();
		if( !$users->toArray() ){
			return "Ainda não há usuários cadastrados!";
		}
		
		# havendo usuários, é só listá-los
		return parent::all();	
	}
	
	/**
	 * 
	 * @param integer $id
	 * @param array $data
	 * @throws ProfileException
	 */
	public function update($id, array $data){
		
		# localizando o usuário (precisa existir)
		if (!parent::find($id)){
			throw new ProfileException("Usuário não encontrado!");
		}
		
		# foi informada senha? criptografe-a!
		if(!empty($data['password'])){
			$data['password'] = bcrypt($data['password']);
		}
		
		# dados de usuário, atualizados
		return parent::update($id, $data);
		
	}
	
	
	/**
	 * 
	 * @param integer $id
	 * @throws ProfileException
	 */
	public function delete($id) {
		
		if (!parent::find($id)){
			throw new ProfileException("Usuário não encontrado!");
		}
		
		# não permitir que o admin seja excluído
		$userNotAdmin = parent::pushCriteria(new UsersNotAdmin())->find($id);
		if ( !$userNotAdmin ){
			throw new ProfileException("Não se pode excluir o administrador do sistema (login: {$userNotAdmin->login})!");
		}
		
		# TODO: verificar se usuário escreveu artigos [falta implementação]
		
		return parent::delete($id);
	}
	
	
	/**
	 * 
	 * @param array $data
	 */
	public function create(array $data){
		$data['password'] = bcrypt($data['password']);
		return parent::create($data);
	}
	
}