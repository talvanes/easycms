<?php namespace EasyCMS\Repositories\Concretes\Criteria;

use EasyCMS\Repositories\CriteriumAbstract;
use EasyCMS\Repositories\Contracts\IRepository;

class UsersNotAdmin extends CriteriumAbstract {
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \EasyCMS\Repositories\CriteriumAbstract::apply()
	 */
	public function apply($model, IRepository $repository){
		$query = $model->where('login', '<>', 'admin');
		return $query;
	}
	
}