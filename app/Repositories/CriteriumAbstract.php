<?php namespace EasyCMS\Repositories;

use EasyCMS\Repositories\Contracts\IRepository;

abstract class CriteriumAbstract {
	public abstract function apply($model, IRepository $repository); 
}
