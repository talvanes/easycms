<?php

namespace EasyCMS\Http\Middleware\Usuario;

use Closure;

class LogoutOnlyMakesSenseOnAuthenticatedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (!$request->user()){
    		return redirect()->route('usuario.login')
    					->with('alert', 'Usuário não autenticado!');
    	}
    	
        return $next($request);
    }
}
