<?php

namespace EasyCMS\Http\Middleware\Usuario;

use Closure;

class OnlyAllowAccessToDashboardForAuthenticatedUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (!$request->user()) 
    		return redirect()->back()
    				->with('error', 'Acesso à área restrita permitido somente a usuários autenticados!');
    	
        return $next($request);
    }
}
