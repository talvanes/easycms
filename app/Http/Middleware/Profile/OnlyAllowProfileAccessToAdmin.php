<?php

namespace EasyCMS\Http\Middleware\Profile;

use Closure;

class OnlyAllowProfileAccessToAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (!$request->user() or $request->user()->level < 3){
    		return redirect()->back()
    				->with('error', 'Acesso restrito somente a usuário com perfil de administrador!');
    	}
    	
        return $next($request);
    }
}
