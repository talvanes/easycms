<?php

namespace EasyCMS\Http\Requests\Usuario;

use EasyCMS\Http\Requests\Request;

class UsuarioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'login' => 'string|unique:EasyCMS.Usuarios,login',
			'password' => 'string|digits_between:6,20',
        ];
    }
}
