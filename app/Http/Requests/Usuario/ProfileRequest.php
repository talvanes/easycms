<?php

namespace EasyCMS\Http\Requests\Usuario;

use EasyCMS\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {	
    	$rules = [
    		'name' => 'string', 
    		'login' => 'min:5|regex:/^[a-zA-Z]*/|unique:Usuarios,login', 
    		'password' => 'between:6,20', 
    		'email' => 'email|unique:Usuarios,email',
    		'level' => 'between:1,3',
    	];
    	
    	// buscando o código de usuário
    	$id = $this->route()->getParameter('profile');
    	// adicionar a cláusula 'required' nas regras de validação
    	if (!isset($id)){
    		# store: todos os campos obrigatórios
    		foreach ($rules as $field => $rule) {
    			$rules[$field] = "required|{$rule}";
    		}
    	} else {
    		# update: apenas os campos informados são obrigatórios
    		$fields = array_keys($this->all());
    		foreach ($fields as $field) {
    			$rules[$field] = 'required|' . $rules[$field];
    			if (in_array($field, ['login', 'email'])){
    				$rules[$field] = $rules[$field] . ',' . $id;
    			}
    		}
    	}
    	
        return $rules;
    }
}
