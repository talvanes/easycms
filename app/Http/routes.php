<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# Tela de login
Route::get('/', ['as' => 'usuario.login', 'uses' => "Usuario\LoginController@login"]);
# Autenticação de usuário
Route::post('/auth', ['as' => 'usuario.auth', 'uses' => "Usuario\LoginController@auth"]);

// Somente para usuários autenticados
Route::group(['middleware' => 'auth.usuario'], function () {
	# Dashboard (área restrita)
	Route::get('/dashboard', ['as' => 'usuario.dashboard', 'uses' => "Usuario\DashboardController@dashboard"]);
});
// Somente para administradores
Route::group(['middleware' => 'auth.admin'], function () {
	# Rotas para administração de usuários (/profile) - somente administradores
	Route::resource('profile', "Usuario\ProfileController", ['except' => ['show', 'create', 'store']]);
});
// Todo mundo pode se cadastrar no sistema de login, mas não haverá opção de permissão administrativa
Route::resource('profile', "Usuario\ProfileController", ['only' => ['create', 'store']]);

Route::group(['middleware' => 'logout.usuario'], function () {
	# Logout (sair da sessão)
	Route::get('/logout', ['as' => 'usuario.logout', 'uses' => "Usuario\LogoutController@logout"]);
});