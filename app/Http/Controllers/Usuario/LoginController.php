<?php

namespace EasyCMS\Http\Controllers\Usuario;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use EasyCMS\Http\Requests;
use EasyCMS\Http\Controllers\Controller;
use EasyCMS\Models\Usuario;

class LoginController extends Controller
{
	/**
	* Exibe a página inicial (home)
	*/
	public function login()
	{
		return view('usuario.home.home');
	}
	
	/**
	* Método para autenticar usuário
	*/
	public function auth(Request $request)
	{
		// credenciais de usuário
		$credenciais = Input::only('login', 'password');
		
		// redirecionar à home (/) login ou senha em branco
		if (empty($credenciais['login']) or empty($credenciais['password'])) {
			return redirect()->route('usuario.login')->with('error', 'Informe usuário e senha!');
		}
		
		$usuario = Usuario::where('login', $credenciais['login'])->first();
		
		// o usuário existe? caso negativo, login inválido: redirecionar à home (/)
		if (!$usuario){
			return redirect()->route('usuario.login')->with('error', 'Usuário inexistente!');
		}
		
		// a senha confere? Ok, prossiga
		if ( Auth::attempt($credenciais) ){
			# verifique se o usuário está inativo
			if ( !$usuario->status ){
				return redirect()->route('usuario.login')->with('error', 'Usuário inativo não pode autenticar!');
			}
		
			# neste momento, todos os testes passaram; então, é só redirecionar à dashboard (área restrita)
			return redirect()->route('usuario.dashboard');
		}
		
		// a senha não confere? redirecionar à home (/)
		return redirect()->route('usuario.login')->with('error', 'Falha na autenticação!');
	}
}
