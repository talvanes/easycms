<?php

namespace EasyCMS\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use EasyCMS\Http\Requests;
use EasyCMS\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
	* Logout (sair da sessão)
	*/
	public function logout(Request $request)
	{
		// somente se o usuário estiver logado, 
		if (Auth::check()) {
			Auth::logout();									# fazer o logout
			return redirect()->route('usuario.login');		# redirecionando para a home (/)
		}
	}
}
