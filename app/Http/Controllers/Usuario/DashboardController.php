<?php

namespace EasyCMS\Http\Controllers\Usuario;

use Illuminate\Http\Request;

use EasyCMS\Http\Controllers\Controller;
use EasyCMS\Http\Requests\Usuario\UsuarioRequest;

class DashboardController extends Controller
{
    /**
	* Acessa a dashboard (área restrita)
	*/
	public function dashboard(UsuarioRequest $request)
	{
		// recuperando dados do usuário logado
		$user = $request->user();
		
		if ($user){	
			// Mensagem de boas-vindas
			return view('usuario.home.dashboard')->with('usuario', $user);
		}
	}
}
