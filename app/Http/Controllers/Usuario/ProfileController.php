<?php

namespace EasyCMS\Http\Controllers\Usuario;

use Illuminate\Http\Request;

use EasyCMS\Http\Requests;
use EasyCMS\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use EasyCMS\Models\Usuario;
use EasyCMS\Http\Requests\Usuario\ProfileRequest;
use Illuminate\Http\JsonResponse;
use EasyCMS\Repositories\Concretes\Repository\ProfileRepository;

class ProfileController extends Controller
{
	/** @var ProfileRepository */
	protected $profileRepository;
	
	/**
	 * ProfileController's default constructor.
	 * @param ProfileRepository $profileRepository
	 */
	public function __construct(ProfileRepository $profileRepository) {
		$this->profileRepository = $profileRepository;
	}
	
	
	/**
	 * Lista todos os usuários cadastrados. Apenas adminsitradores podem visualizar.
	 */
	public function index()
	{
		#listagem simples de usuários
		$return = $this->profileRepository->listAllUsers();
		return new JsonResponse($return);
	}
	
	
	/**
	 * Exibe o formulário de cadastro.
	 */
	public function create()
	{
		// TODO: formulário de registro (link "Sign up" ou "Register")
		$levels = [
			1 => 'Usuário comum',
			2 => 'Gestor',
			3 => 'Sys Admin',
		];
		
		return view('usuario.profile.create.index', compact('levels'));
	}
	
	/**
	 * Faz o cadastro de usuários. Apenas administradores podem acessar o recurso.
	 * @param Request $request
	 */
	public function store(ProfileRequest $request)
	{
		# dados de usuário
		$dados = $request->all();
		
		# enviando o usuário recém-criado
		$result = $this->profileRepository->create($dados);
		return new JsonResponse($result);
	}
	
	
	/**
	 * Exibe o formulário de edição. Apenas administradores podem visualizá-lo.
	 * @param Request $request
	 * @param integer $id
	 */
	public function edit(Request $request, $id)
	{
		
	}
	
	/**
	 * Faz a atualização de usuários. Apenas administradores podem acessar o recurso.
	 * @param Request $request
	 * @param integer $id
	 */
	public function update(ProfileRequest $request, $id)
	{
		# dados de usuário
		$dados = $request->all();
		
		# retornando os dados de usuário, atualizados
		$return = $this->profileRepository->update($id, $dados);
		return new JsonResponse($return);
		
	}
	
	/**
	 * Exclui cadastro de usuário que não tenha artigos associados. Apenas administradores têm acesso ao recurso. 
	 * @param integer $id
	 */
	public function destroy($id)
	{
		# TODO: verificação de artigos publicados
		
		$result = $this->profileRepository->delete($id);
		return new JsonResponse($result);
	}
	
}
