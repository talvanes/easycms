<?php namespace EasyCMS\Usuario;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;

class UsuarioTest extends TestCase
{
	use WithoutMiddleware, DatabaseTransactions;
	
    /**
     * O sistema acessa a home (/)?
     * O teste espera um ACERTO.
	 * 
     * @return void
     */
    public function testShouldAccessHomePage()
    {
        $this->visit('/')
			->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Login não informado está redirecionando para a / (home)?
     * O teste espera um ERRO.
	 * 
     * @return void
     */
    public function testShouldFailOnBlankLogin()
    {
        # credenciais de usuário: login de usuário em branco
		$credenciais = [
			'login' => null,
		];
		
		# autenticando as credenciais (POST na URL /auth)
		$response = $this->post('/auth', $credenciais);
		
		# redirecionar para a home (/) em caso de erro
		$response->assertRedirectedTo('/')
			->followRedirects()
			->assertSessionHas('error', 'Informe usuário e senha!');
		
		# agora devo estar na home (/)
		$this->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Senha não informada está redirecionando para a / (home)?
     * O teste espera um ERRO.
	 * 
     * @return void
     */
    public function testShouldFailOnBlankPassword()
    {
        # credenciais: senha em branco
		$credenciais = [
			'login' => str_random(rand(5,10)),
			'password' => null,
		];
		
		# autenticando as credenciais (POST na URL /auth)
		$response = $this->post('/auth', $credenciais);
		
		# redirecionar para a home (/) em caso de erro
		$response->assertRedirectedTo('/')
			->followRedirects()
			->assertSessionHas('error', 'Informe usuário e senha!');
		
		# agora preciso estar na home (/)
		$this->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Usuário inválido (não existe no sistema) está sendo redirecionado para a / (home), com erros?
     * O teste espera um ERRO.
	 * 
     * @return void
     */
    public function testShouldFailOnInvalidLogin()
    {
        # credenciais de usuário inexistente
		$usuario = factory(\EasyCMS\Models\Usuario::class)->make();
		
		$senha = str_random(rand(6,20));	// uma senha entre 6 a 20 caracteres
		$credenciais = [
			'login' => $usuario->login,
			'password' => $senha,			# neste caso, a senha NÂO importa
		];
		
		# tente autenticar o usuário
		$response = $this->post('/auth', $credenciais);
		
		# redirecionar para a home (/) em caso de erro
		$response->assertRedirectedTo('/')
			->followRedirects()
			->assertSessionHas('error', 'Usuário inexistente!');
		
		# agora preciso estar na home (/)
		$this->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Senha inválida (não confere) está sendo redirecionado para a / (home), com erros?
     * O teste espera um ERRO.
	 * 
     * @return void
     */
    public function testShouldFailOnWrongPassword()
    {
        # garantindo que o usuário esteja cadastrado no sistema
		$senha = str_random(16);		// uma senha qualquer de 16 caracteres
		$usuario = factory(\EasyCMS\Models\Usuario::class)->create([
			'password' => bcrypt($senha),
		]);
		
		# credenciais
		$credenciais = [
			'login' => $usuario->login,
			// a senha não confere
			'password' => str_random(15)	// a senha informada é diferente do cadastro
		];
		
		# autenticando o usuário
		$response = $this->post('/auth', $credenciais);
		
		# redirecionando para a home (/) em caso de erro
		$response->assertRedirectedTo('/')
			->followRedirects()
			->assertSessionHas('error', 'Falha na autenticação!');
		
		# agora preciso estar na home (/)
		$this->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Usuário inativo está sendo redirecionado para a / (home), com erros?
     * O teste espera um ERRO.
	 * 
     * @return void
     */
    public function testShouldFailOnInactiveUser()
    {
        # o usuário cadastrado está inativo
		$senha = str_random(rand(6,20));		// uma senha entre 6 e 20 caracteres
		$usuario = factory(\EasyCMS\Models\Usuario::class)->create([
			'password' => bcrypt($senha),
			'status' => 0,						// usuário foi criado desativado
		]);
		
		# credenciais
		$credenciais = [
			'login' => $usuario->login,
			'password' => $senha,
		];
		
		# autenticando o usuário
		$response = $this->post('/auth', $credenciais);
		
		# redirecionando para a home (/) em caso de erro
		$response->assertRedirectedTo('/')
			->followRedirects()
			->assertSessionHas('error', 'Usuário inativo não pode autenticar!');
		
		# agora preciso estar na home (/)
		$this->see('Login')
			->dontSee('Bem-vindo');
    }
	
	/**
     * Usuário ativo, que digitou as credenciais corretamente, está sendo redirecionado para a dashboard?
     * O teste espera um ACERTO.
	 * 
     * @return void
     */
    public function testShouldSucceedOnActiveUser()
    {
		# o usuário cadastrado está devidamente ativo
		$senha = str_random(rand(6,20));				// a senha contèm entre 6 e 20 caracteres
        $usuario = factory(\EasyCMS\Models\Usuario::class)->create([
			'password' => bcrypt($senha),
        	'remember_token' => $senha,
		]);
		
		# credenciais
		$credenciais = [
			'login' => $usuario->login,
			'password' => $senha,
		];
		
		# autenticando o usuário
		$response = $this->post('/auth', $credenciais);
		
		# redirecionando o usuário para a área restrita (/dashboard) com sucesso
		$response->assertRedirectedTo('/dashboard')
			->followRedirects()
			->assertResponseOk();
			
		# para garantir, preciso estar na área restrita (/dashboard)
		$this->see("Bem-vindo, {$usuario->name}")
			->dontSee('Login');
    }
	
	/**
     * Usuário logado saiu da sessão com sucesso, sendo também redirecionado para a / (home)?
     * O teste espera um ACERTO.
	 * 
     * @return void
     */
    public function testShouldSucceedOnLogout()
    {
        # sendo que este usuário existe...
        $usuario = factory(\EasyCMS\Models\Usuario::class)->create();
		
		# .. e já está logado, fazê-lo sair da sessão
		$this->actingAs($usuario)
			# clique no link "Sair" para consumir a rota abaixo
			->visit('/logout')
			# agora preciso estar na home (/)
			->see('Login')
			->dontSee('Bem-vindo');
		
    }
	
	
}
