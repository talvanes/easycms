<?php namespace EasyCMS\Tests\Usuario\Profile;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use EasyCMS\Models\Usuario;
use TestCase;

class UpdateTest extends TestCase
{
	use WithoutMiddleware;
	use DatabaseTransactions;
	
    /**
     * Este teste deveria dar certo ao enviar dados válidos.
     *
     * @return void
     */
    public function testShouldSucceedOnCorrectData()
    {
        # este usuário existe
        $usuario = factory(Usuario::class)->create();
        # estes dados estão corretos
        $dados = [
        	'name' => str_random(rand(15,25)),
        	'login' => array_rand(['A','E','I','O','U']) . str_random(rand(4,9)),
        	'password' => str_random(rand(6,20)),
        	'email' => str_random(rand(4,10)) . '@email.com',
        ];
        
        // solicitar a rota 'profile.update'
        $response = $this->call('PUT', route('profile.update', $usuario->id), $dados);
        
        // espero a atualização dos dados
        $this->assertResponseOk();
        $this->seeInDatabase('Usuarios', ['id' => $usuario->id, 'login' => $dados['login'], 'email' => $dados['email']]);
    }
    
    /**
     * Este teste deveria dar errado com envio de nome em branco.
     *
     * @return void
     */
    public function testShouldFailOnBlankName()
    {
    	# este usuário existe
    	$usuario = factory(Usuario::class)->create();
    	# nome está em branco
    	$dados = ['name' => null];
    	
    	// solicitar a rota 'profile.update'
    	$response = $this->call('PUT', route('profile.update', $usuario->id), $dados);
    	
    	// os dados não podem ser atualizados, ou seja, espero um erro
    	$this->assertNotEquals(200, $response->getContent());
    }
    
    /**
     * Este teste deveria dar errado com envio de senha inválida.
     *
     * @return void
     */
    public function testShouldFailOnInvalidPassword()
    {
    	# este usuário existe
    	$usuario = factory(Usuario::class)->create();
    	# senha é inválida
    	$dados = ['password' => str_random(21)];
    	 
    	// solicitar a rota 'profile.update'
    	$response = $this->call('PUT', route('profile.update', $usuario->id), $dados);
    	 
    	// os dados não podem ser atualizados, ou seja, espero um erro
    	$this->assertNotEquals(200, $response->getContent());
    }
    
    /**
     * Este teste deveria dar errado com envio de identificação de usuário (login) inválida.
     *
     * @return void
     */
    public function testShouldFailOnInvalidLogin()
    {
    	# este usuário existe
    	$usuario = factory(Usuario::class)->create();
    	# login inválido
    	$dados = ['login' => rand(0,9) . str_random(3)];
    	 
    	// solicitar a rota 'profile.update'
    	$response = $this->call('PUT', route('profile.update', $usuario->id), $dados);
    	 
    	// os dados não podem ser atualizados, ou seja, espero um erro
    	$this->assertNotEquals(200, $response->getContent());
    }
    
    /**
     * Este teste deveria dar errado com envio de e-mail em branco.
     *
     * @return void
     */
    public function testShouldFailOnInvalidEmail()
    {
    	# este usuário existe
    	$usuario = factory(Usuario::class)->create();
    	# email inválido
    	$dados = ['email' => str_random(rand(15,25))];
    	 
    	// solicitar a rota 'profile.update'
    	$response = $this->call('PUT', route('profile.update', $usuario->id), $dados);
    	 
    	// os dados não podem ser atualizados, ou seja, espero um erro
    	$this->assertNotEquals(200, $response->getContent());
    }
}
