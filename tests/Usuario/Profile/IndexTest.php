<?php namespace EasyCMS\Tests\Usuario\Profile;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use EasyCMS\Models\Usuario;
use TestCase;

class IndexTest extends TestCase
{
	use WithoutMiddleware, DatabaseTransactions;
	
    /**
     * Este teste deveria listar os usuários cadastrados para o perfil administrador.
     * Espera-se um ACERTO. [OK]
     *
     * @return void
     */
    public function testShouldListUsersAsAdmin()
    {
        try {
        	// um usuário administrador:
        	$admin = Usuario::create([
        		'name' => 'Administrador do Sistema',
        		'login' => 'admin',
        		'email' => 'admin@email.com',
        		'password' => bcrypt(str_random(rand(6,20))),
        		'status' => 1,
        		'level' => 3,
        	]);
        	// criando outros usuários para listagem (entre 1 e 10 a mais)
        	factory(Usuario::class, rand(1,10))->create();
        	
        	// consumir a rota 'profile.index' para listar os usuários
        	$response = $this->actingAs($admin)->get(route('profile.index'));
        	
        	// listar os usuários APENAS para o adminstrador
        	$this->assertEquals(200, $response->response->getStatusCode());
        	
        } catch (\Exception $e) {
        	$this->assertTrue(false, "Exception {$e->getMessage()} on file {$e->getFile()}, line no. {$e->getLine()}");
        }
    }
    
    /**
     * Este teste não deveria listar usuários para outros usuários (ex. editores, tradutores, convidados, etc.).
     * Espera-se um ERRO. [OK]
     * 
     * @return void
     */
    public function testShouldNotListUsersAsAnother()
    {
    	try {
    		// criando outros usuários para listagem (entre 1 e 10 a mais)
    		$users = factory(Usuario::class, rand(1,10))->create();
    		// um usuário qualquer na sequência (não é o administrador)
    		$user = $users->first();
    		
    		// consumir a rota 'profile.index' para listar os usuários
    		$response = $this->actingAs($user)->get(route('profile.index'));
    		
    		// NÃO listar usuários do sistema para usuário comum
    		$this->assertNotEquals(200, $response->response->getStatusCode());
    		
    	} catch (\Exception $e) {
    		$this->assertTrue(false, "Exception {$e->getMessage()} on file {$e->getFile()}, line no. {$e->getLine()}");
    	}
    }
    
    /**
     * Este teste deveria listar apenas os usuários comuns.
     * Por medida de segurança, o sistema NÂO vai listar o usuário administrador registrado na sessão. [OK]
     * 
     *  @return void
     */
    public function testShouldListOnlyRegularUsers()
    {
    	try {
    		// usuário administrador
    		$admin = Usuario::create([
    			'name' => 'Administrador do Sistema',
    			'login' => 'admin',
    			'email' => 'admin@email.com',
    			'password' => bcrypt(str_random(rand(6,20))),
    			'status' => 1,
    			'level' => 3,
    		]);
    		// um usuário comum
    		$usuario = factory(Usuario::class)->create();
    		
    		// consumir a rota 'profile.index' para listar usuários
    		$response = $this->actingAs($admin)->get(route('profile.index'));
    		
    		// Ver o usuário comum, mas não o administrador
    		$response
    			->seeJson(['login' => $usuario->login])
    			->dontSeeJson(['login' => 'admin']);
    		
    	} catch (\Exception $e) {
    		$this->assertTrue(false, "Exception {$e->getMessage()} on file {$e->getFile()}, line no. {$e->getLine()}");
    	}
    }
    
}
