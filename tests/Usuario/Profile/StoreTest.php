<?php namespace EasyCMS\Tests\Usuario\Profile;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use EasyCMS\Models\Usuario;
use TestCase;

class StoreTest extends TestCase
{
	use WithoutMiddleware;
	use DatabaseTransactions;
	
	/**
	 * Este teste deveria aceitar os campos inseridos corretamente.
	 * 
	 * @return void
	 */
    public function testShouldSucceedOnValidData(){
    	# senha correta, plana
    	$senha = str_random(rand(6,20));
    	# usuário com as credenciais corretas (ainda não foi criado)
    	$usuario = factory(Usuario::class)->make(['password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'name' => $usuario->name,
    		'login' => $usuario->login,
    		'email' => $usuario->email,
    		'level' => $usuario->level,
    		'password' => $senha,
    	];
    	
    	# solicitando a rota 'profile.store'
    	$response = $this->call('POST', route('profile.store'), $dados);
    	# recuperando o usuário criado
    	$usuario = json_decode($response->getContent());
    	
    	# espera-se um acerto
    	$this->assertEquals(200, $response->getStatusCode());
    	# o usuário precisa estar inserido
    	$this->seeInDatabase('Usuarios', ['id' => $usuario->id]);
    	$this->seeJson(['id' => $usuario->id]);
    }
    
    /**
     * Este teste também deveria aceitar inserção de dados de outro usuário adimnistrador
     * 
     * @return void
     */
    public function testShouldSucceedOnInsertingAnotherAdmin(){
    	# senha correta, plana
    	$senha = str_random(6, 20);
    	# usuário com as credenciais corretas
    	$usuario = factory(Usuario::class)->make(['level' => 3, 'password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'login' => $usuario->login,
    		'name' => $usuario->name,
    		'level' => $usuario->level,
    		'email' => $usuario->email,
    		'password' => $senha,
    	];
    	
    	// solicitando a gravação dos dados
    	$response = $this->call('POST', route('profile.store'), $dados);
    	# recuperando o usuário criado
    	$usuario = json_decode($response->getContent());
    	
    	// o que espero? que os dados sejam inseridos corretamente, sem erros
    	$this->assertEquals(200, $response->getStatusCode());
    	# e que estejam no banco de dados
    	$this->seeInDatabase('Usuarios', ['id' => $usuario->id, 'level' => 3]);
    	$this->seeJson(['id' => $usuario->id, 'level' => 3]);
    }
    
    /**
     * Este teste deveria falhar quando não se preenche o nome.
     * 
     * @return void
     */
    public function testSouldFailOnMissingName(){
    	# senha correta, plana
    	$senha = str_random(rand(6,20));
    	# usuário sem o nome preenchido (ainda não foi criado)
    	$usuario = factory(Usuario::class)->make(['name' => null, 'password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'name' => $usuario->name,
    		'login' => $usuario->login,
    		'email' => $usuario->email,
    		'password' => $senha,
    	];
    	
    	# solicitando a rota 'profile.index'
    	$response = $this->call('POST', route('profile.store'), $dados);
    	
    	# espera-se um erro
    	$this->assertNotEquals(200, $response->getContent());
    	# o usuário não pode ser inserido
    	$this->dontSeeInDatabase('Usuarios', ['id' => $usuario->id]);
    }
    
    /**
     * Este teste deveria falhar quando a senha é inválida.
     * 
     * @return void
     */
    public function testShouldFailOnInvalidPassword(){
    	# senha digitada (4 dígitos)
    	$senha = str_random(4);
    	# usuário prrencheu senha inválida (ainda não foi criado)
    	$usuario = factory(Usuario::class)->make(['password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'name' => $usuario->name,
    		'login' => $usuario->login,
    		'email' => $usuario->email,
    		'password' => $senha,
    	];
    	 
    	# solicitando a rota 'profile.index'
    	$response = $this->call('POST', route('profile.store'), $dados);
    	 
    	# espera-se um erro
    	$this->assertNotEquals(200, $response->getContent());
    	# o usuário não pode ser inserido
    	$this->dontSeeInDatabase('Usuarios', ['id' => $usuario->id]);
    }
    
    /**
     * Este teste deveria falhar quando a identificação de usuário (login) é inválida.
     * 
     * @return void
     */
    public function testShouldFailOnInvalidLogin(){
    	# login inválido
    	$login = rand(0,9) . str_random(3); 
    	# senha correta, plana
    	$senha = str_random(rand(6,20));
    	
    	# usuário preencheu login inválido (ainda não foi criado)
    	$usuario = factory(Usuario::class)->make(['login' => $login, 'password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'name' => $usuario->name,
    		'login' => $usuario->login,
    		'email' => $usuario->email,
    		'password' => $senha,
    	];
    	 
    	# solicitando a rota 'profile.index'
    	$response = $this->call('POST', route('profile.store'), $dados);
    	 
    	# espera-se um erro
    	$this->assertNotEquals(200, $response->getContent());
    	# o usuário não pode ser inserido
    	$this->dontSeeInDatabase('Usuarios', ['id' => $usuario->id]);
    }
    
    /**
     * Este teste deveria falhar quando o e-mail é inválido.
     * 
     * @return void
     */
    public function testShouldFailOnInvalidEmail(){
    	# email inválido
    	$email = str_random(15,30);
    	# senha correta, plana
    	$senha = str_random(rand(6,20));
    	
    	# usuário preencheu email inválido (ainda não foi criado)
    	$usuario = factory(Usuario::class)->make(['email' => $email, 'password' => bcrypt($senha)]);
    	# dados
    	$dados = [
    		'name' => $usuario->name,
    		'login' => $usuario->login,
    		'email' => $usuario->email,
    		'password' => $senha,
    	];
    	
    	# solicitando a rota 'profile.index'
    	$response = $this->call('POST', route('profile.store'), $dados);
    	
    	# espera-se um erro
    	$this->assertNotEquals(200, $response->getContent());
    	# o usuário não pode ser inserido
    	$this->dontSeeInDatabase('Usuarios', ['id' => $usuario->id]);
    }
    
}
