<?php namespace EasyCMS\Tests\Usuario\Profile;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use EasyCMS\Models\Usuario;
use TestCase;
use Illuminate\Routing\Route;

class DestroyTest extends TestCase
{
	use WithoutMiddleware;
	use DatabaseTransactions;
	
    /**
     * Este teste não deveria excluir usuário que não existe.
     *
     * @return void
     */
    public function testNaoExcluirUsuarioNaoExiste()
    {
        # este usuário não existe
        $usuario = factory(Usuario::class)->make();
        
        // excluir usuário
        $response = $this->call('DELETE', route('profile.destroy', $usuario->id));
        
        // espera-se um erro
        $this->assertNotEquals(200, $response->getStatusCode());
    }
    
    /**
     * Este teste precisa excluir usuário que existe no sistema,
     * desde que ele não tenha artigos no sistema [falta implementação].
     *
     * @return void
     */
    public function testExcluirUsuarioExiste()
    {
    	#este usuário existe
    	$usuario = factory(Usuario::class)->create();
    	
    	// excluir usuário
    	$response = $this->call('DELETE', route('profile.destroy', $usuario->id));
    	
    	// espera-se um acerto
    	$this->assertEquals(200, $response->getStatusCode());
    	$this->dontSeeInDatabase('Usuarios', [ 'id' => $usuario->id ]);
    }
    
    /**
     * Este teste não deveria excluir o administrador do sistema (login: admin).
     *  
     *  @return void
     */
    public function testNaoExcluirAdministrador(){
    	# o administrador do sistema
    	$admin = Usuario::where('login', 'admin')->first();
    	
    	// solicitando a exclusão do administrador (tentativa)
    	$response = $this->call('DELETE', route('profile.destroy', $admin->id));
    	
    	// o que espero? um erro que não permita a exclusão do admininstrador
    	$this->assertNotEquals(200, $response->getStatusCode());
    	$this->seeInDatabase('Usuarios', ['login' => 'admin']);
    }
}
