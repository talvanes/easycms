<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use EasyCMS\Models\Usuario;
use EasyCMS\Repositories\Concretes\ProfileRepository;

class TransactionTests extends TestCase
{
	use WithoutMiddleware, DatabaseTransactions;
	
    /**
     * This test should insert a record onto database.
     * [create() method]
     *
     * @return void
     */
    public function testShouldCreateRecord()
    {
        // be all the user data
        $userData = factory(Usuario::class)->make()->toArray();
        $userData['password'] = bcrypt(str_random(rand(6,20)));
        
        // I must insert it into database by running method create()
        $user = app(ProfileRepository::class)->create($userData);
        
        // what do I expect? to get the record inserted and it gives me an id
        $this->assertNotNull($user);
        $this->seeInDatabase('Usuarios', ['id' => $user->id]);
        
    }
    
    /**
     * This test should update an existing record given its id.
     * [update() method]
     *
     * @return void
     */
    public function testShouldUpdateRecord()
    {
    	// be the whole user list
    	$usuarios = factory(Usuario::class, 10)->create();
    	
    	// and some data to update a random user to
    	$randomUser = $usuarios->first();
    	
    	$userData = factory(Usuario::class)->make()->toArray();
    	$userData['password'] = bcrypt(str_random(rand(6,20)));
    	
    	// let's run method update() to update $randomUser's data
    	$result = app(ProfileRepository::class)->update($randomUser->id, $userData);
    	
    	// what do I expect? to receive the user info updated
    	$this->assertNotEquals($randomUser, $result);
    	$this->seeInDatabase('Usuarios', ['id' => $randomUser->id, 'login' => $userData['login'], 'password' => $userData['password']]);
    	
    }
    
    /**
     * This test should delete record from database.
     * [delete() method] 
     *
     * @return void
     */
    public function testShouldDeleteRecord()
    {
    	// be the whole entire user list
    	$usuarios = factory(Usuario::class, 10)->create();
    	
    	// pick up one record to be deleted
    	$usuario = $usuarios->first();
    	
    	// let's now delete the record by using the method delete()
    	app(ProfileRepository::class)->delete($usuario->id);
    	
    	// I expect such record not to exist anymore
    	$this->dontSeeInDatabase('Usuarios', ['id' => $usuario->id]);
    	
    }
}
