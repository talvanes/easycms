<?php namespace EasyCMS\Tests\Repository;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use EasyCMS\Repositories\Concretes\ProfileRepository;
use EasyCMS\Models\Usuario;
use TestCase;

class IndexTests extends TestCase
{
	use WithoutMiddleware, DatabaseTransactions;
	
	/**
	 * This test should list all the records in database.
	 * [all() method]
	 *
	 * @return void
	 */
	public function testShouldListAll()
	{
		// retrieving users
		$usuarios = factory(Usuario::class, 10)->create();
		
		// running method all()
		$resultSet = app(ProfileRepository::class)->all();
		
		// what do I expect? to get all users
		$this->assertEquals($usuarios->count(), $resultSet->count());
		
	}
	
	/**
	 * This test should list only the number of records per page given.
	 * [paginate() method]
	 *
	 * @return void
	 */
	public function testShouldListRecordsPerPage()
	{
		// records per page
		$recordsPerPage = rand(1, 50);
		 
		// retrieving all users
		$usuarios = factory(Usuario::class, 50)->create();
		 
		// running method paginate()
		$resultSet = app(ProfileRepository::class)->paginate($recordsPerPage);
		 
		// what do I expect? to list just the number of records given by variable $recordsPerPage 
		$this->assertEquals($recordsPerPage, $resultSet->count());
		 
	}
	
	/**
	 * This test should find records by their primary key attribute (id).
	 * [find() method]
	 *
	 * @return void
	 */
	public function testShouldFindById()
	{
		 // be the whole user list
		 $usuarios = factory(Usuario::class, 10)->create();
		 
		 // let's (try to) find out a known user id
		 $usuario = $usuarios->first();
		 
		 // running method find()
		 $result = app(ProfileRepository::class)->find($usuario->id);
		 
		 // what do I expect? to get the same user by its own id
		 $this->assertEquals($usuario->id, $result->id);
	}
	
	/**
	 * This test should find records by another attribute.
	 * [findBy() method]
	 *
	 * @return void
	 */
	public function testShouldFindByAttribute()
	{
		 // be the whole user list
		 $usuarios = factory(Usuario::class, 10)->create();
		 
		 // let's (try to) find out a known user login
		 $usuario = $usuarios->first();
		 
		 // running method findBy()
		 $result = app(ProfileRepository::class)->findBy('login', $usuario->login);
		 
		 // what do I expect? to get the same user data as a result
		 $this->assertEquals($usuario->login, $result->login);
		 
	}
}
