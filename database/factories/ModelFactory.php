<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(EasyCMS\Models\Usuario::class, function (Faker\Generator $faker) {
	# senha plana de 6 a 20 caracteres
	$password = $faker->password;
	
    return [
        'name' => $faker->name,
		'login' => $faker->userName,
        'email' => $faker->safeEmail,
        'password' => bcrypt($password),
        'remember_token' => ($faker->boolean())? $password : null,
		'status' => 1,
    	'level' => ['1', '2'][rand(0, 1)],
    ];
});
