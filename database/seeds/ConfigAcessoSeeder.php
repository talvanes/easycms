<?php

use Illuminate\Database\Seeder;
use EasyCMS\Models\ConfigAcessos;

class ConfigAcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Users
        ConfigAcessos::create([
        	'name' => 'user',
        	'description' => 'User permission group',
        ]);
        // Managers
        ConfigAcessos::create([
        	'name' => 'manager',
        	'description' => 'Manager permission group',
        ]);
        // SysAdmins
        ConfigAcessos::create([
        	'name' => 'admin',
        	'description' => 'System Administrators',
        ]);
    }
}
