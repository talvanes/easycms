<?php

use Illuminate\Database\Seeder;
use EasyCMS\Models\Usuario;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # administrador (admin)
        Usuario::create([
        	'name' => 'System Administrator',
        	'email' => 'admin@email.com',
        	'login' => 'admin',
        	'password' => Hash::make('admin'),
        	'level' => 3,
        	'status' => 1,
        ]);
    }
}
